#!/usr/bin/env bash
# Shows a run menu with calculator and bash alias support.
# Requirements:
#   - bash
#   - rofi
#   - bc, python (either version 2 or 3) or bcalc for calculations
# Example input: geany, 2+2, 2-1, 10/3, 2*2=, (3^2)*10, some-alias

CURRDIR=$(dirname $0)
rofi -combi-modi "exec:${CURRDIR}/rofi-exec.sh",drun,"files:${CURRDIR}/rofi-files.sh","aliases:${CURRDIR}/rofi-aliases.sh","bookmarks:${CURRDIR}/rofi-bookmarks.sh" -show combi
