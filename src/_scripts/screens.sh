#!/usr/bin/env bash

# Offer a menu to choose the screen to connect
chosen=$( xrandr | grep " connected" | rofi -dmenu -i )

# Switch to screen that was selected.
if [[ $chosen != "" ]]; then
	# We check if the screen is already turned on
	# when connected it should have a resolution on the line
	resolutionchar=$( echo $chosen | awk '{print substr ($3, 0, 1)}' )
	# We check if the first character is a number to
	# determine if it is a resolution
	case $resolutionchar in
		''|*[!0-9]*) turnedon=false ;;
		*) turnedon=true ;;
	esac

	# Get the screen ID from first column
	screen=$( echo $chosen | awk '{print $1}' )

	# Enable it for output or disable if already enabled
	if [[ $turnedon == false ]]; then
		xrandr --output $screen --auto
	else
		xrandr --output $screen --off
	fi
fi
