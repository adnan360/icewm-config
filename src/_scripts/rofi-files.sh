#!/usr/bin/env bash

# A rofi mode script that lets you open directories and files

# Change prompt
echo -en "\0prompt\x1fopen\n"

# Save the input path for later
input_expanded="$(echo "$@" | sed "s|^\~|$HOME|")"

# Handle input
if [ -d "$input_expanded" ]; then
	coproc ( thunar "$input_expanded" )
    exit 0
elif [ -f "$input_expanded" ]; then
	coproc ( xdg-open "$input_expanded" )
    exit 0
fi

# List files and directories
# Idea from: https://askubuntu.com/a/427290
find $HOME -maxdepth 2 -not -path '*/\.*'  \( ! -iname ".*" \) | sed "s|$HOME|~|"
