# icewm-config

Simple IceWM config that mimics a traditional desktop environment.

![Screenshot 01](https://codeberg.org/adnan360/icewm-config/raw/branch/main/assets/screenshot-01.png "A screenshot of this IceWM config")

## Install

Step 1: Optionally make edits to `defaults.yaml`.

Step 2: Install recommended packages:

- `icewm` (of course!)
- `bash` (for scripts to work, such as run menu, popup terminal)
- `rofi` (to show run menu & other menus)
- `papirus-icon-theme` (to show nice icons; use `scripts/changeicon.sh Papirus` to change; this exact theme is not an absolute requirement, you can use any other icon theme)
- `i3lock` (to lock screen)
- `xdotool` & `wmctrl` (for popup terminal to be resized, placed, focused etc.)
- `xcalendar` (to show calendar when the clock is clicked)

Install other programs set in `defaults.yaml` to have a functional desktop setup.

Step 3: Run:

```sh
./install.pl
```

Afterwards logout and login to an IceWM session.

## License

Code exclusive to this repo is available under CC0 1.0 Universal license. See `LICENSE.md` for details.
