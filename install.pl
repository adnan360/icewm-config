#!/usr/bin/env perl

use warnings;
use strict;

# For dirname
use File::Basename;

# To let user run this script from any directory
# letting the script find files relative to script dir
chdir dirname(__FILE__);

# Use modules from local dir
# dirname part is to let user run this script from any directory
use lib dirname(__FILE__) . '/perlmods';

# To process YAML
use YAML::Tiny;

# Templating engine
use Text::Template;

# To copy files
use File::Copy;

# For relative path
use File::Spec;

# For creating directories
use File::Path qw(make_path);

# Prepare path variables to be used later
my $icewm_home_dir = join('', $ENV{'HOME'}, '/.icewm');
my $icewm_share_dir = join('', $ENV{'HOME'}, '/.local/share/icewm-config');
my $user_config_dir = join('', $ENV{'HOME'}, '/.config');

# Read config yaml file with overrides possible
# Summary:
# 1. defaults.yaml is used normally
# 2. if /tmp/defaults.yaml found, move it to ~/.local/share
# 3. if defaults.yaml is found on ~/.local/share, use it
#
# Normal case (1)
my $config_file = 'defaults.yaml';
# If a copy of defaults.yaml is put in /tmp, for config editing purpose
# move it to ~/.local/share so that it is considered next time
# instead of defaults.yaml from script dir (2)
if ( -f '/tmp/defaults.yaml' ) {
	move('/tmp/defaults.yaml', "${icewm_share_dir}/defaults.yaml");
}
# Use file from ~/.local/share if found (3)
if ( -f "${icewm_share_dir}/defaults.yaml" ) {
	$config_file = "${icewm_share_dir}/defaults.yaml";
}
# Show config file path that is chosen
print "= using config file: ${config_file} \n";
# Read the yaml file
my $yaml = YAML::Tiny->read( $config_file );

# List files in dir and subdir
use File::Find;

sub write_file_contents {
	my $string = shift;
	my $filepath = shift;

	open(my $file_handler, '>', $filepath) or die $!;
	print $file_handler $string;
	close($file_handler);
}

sub parse_template {
	my $source = shift;
	my $destination = shift;

	my $template = Text::Template->new(TYPE => 'FILE',  SOURCE => $source);
	my $text = $template->fill_in(HASH => @_, DELIMITERS => [ '{__', '__}' ]);
	write_file_contents($text, $destination);
}

sub make_directory {
	my $dir = shift;
	if ( ! -d $dir ) {
		make_path( $dir, {error => \my $err} );
		if ( $err && @$err ) {
			die("Could not make directory: $dir");
		} else {
			return 0;
		}
	} else {
		return 0;
	}
}

my @dir_content;
sub process_found_file {
	push @dir_content, $File::Find::name;
	return;
};
sub get_dir_content {
	@dir_content = ();
	finddepth(\&process_found_file, shift);
	return @dir_content;
}

# Files for ~/.icewm
my $dest_file;
my $dest_dir;
make_directory( $icewm_home_dir );
foreach my $file (get_dir_content('src/_icewm')) {
	if ( -f "$file" ) { # to get rid of .. and . items
		$dest_file = $icewm_home_dir . '/' . File::Spec->abs2rel($file, 'src/_icewm');
		$dest_dir = dirname($dest_file);
		unless ( -d "$dest_dir" ) { make_directory( "$dest_dir" ); }
		print("= prepping file: $dest_file \n");

		parse_template("$file", "$dest_file", @{$yaml});
	}
}

# Files for ~/.local/share/icewm-config
make_directory( $icewm_share_dir );
foreach my $file (get_dir_content('src/_scripts')) {
	if ( -f $file ) { # to get rid of .. and . items
		$dest_file = $icewm_share_dir . '/' . File::Spec->abs2rel($file, 'src/_scripts');
		$dest_dir = dirname($dest_file);
		unless ( -d $dest_dir ) { make_directory( $dest_dir ); }
		print("= prepping file: $dest_file \n");

		parse_template("$file", "$dest_file", @{$yaml});
		chmod 0755, "$dest_file"
			or die "Could not change file permission for: ${file}";
	}
}

# Files for ~/.config
make_directory( $user_config_dir );
foreach my $file (get_dir_content('src/_config')) {
	if ( -f "$file" ) { # to get rid of .. and . items
		$dest_file = $user_config_dir . '/' . File::Spec->abs2rel($file, 'src/_config');
		$dest_dir = dirname($dest_file);
		unless ( -d "$dest_dir" ) { make_directory( "$dest_dir" ); }
		print("= prepping file: $dest_file \n");

		parse_template("$file", "$dest_file", @{$yaml});
	}
}
