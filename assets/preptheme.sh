#!/usr/bin/env bash

# Check for required binaries beforehand
if [ -z "$(command -v inkscape)" ] || [ -z "$(command -v convert)" ]; then
	echo 'This script needs inkscape and ImageMagick (for convert binary)'
	exit 11
fi

if ps ax -o 'command' | grep -q '^inkscape\s.*assets\/theme\.svg'; then
	echo 'WARNING! theme.svg is opened in Inkscape.'
	echo 'Press ENTER to continue. To cancel this script, press Ctrl+C...'
	read
fi

cd $(dirname $0)
export_dir="$PWD/export"
icewm_config_path="../src/_icewm"
theme_path="${icewm_config_path}/themes/trad"

# Convert an image file
#  $1: Input file
#  $2: Output file
function convert_image {
	filename="$(basename ${1%.*})"
	echo "= converting image: ${filename} ..."
	convert "$1" -colorspace LAB "$2"
}

# Convert and resize an image file to xpm
#  $1: Input file
#  $2: Output xpm file
#  $3: Output width in pixels
#  $4: Output height in pixels
function convert_image_resize {
	filename="$(basename ${1%.*})"
	echo "= converting image: ${filename} ..."
	# ImageMagick seems to add exif data which changes even if the image did
	# not change. -strip added so that same image does not trigger a diff.
	convert "$1" -strip -colorspace LAB -resize "${3}x${4}" "$2"
}

echo "= exporting slice pngs ..."
if [ ! -d "$export_dir" ]; then mkdir -p "$export_dir"; fi
# Window related slices
window_slices=(
"closeA"
"maximizeA"
"restoreA"
"minimizeA"
"rollupA"
"rolldownA"
"depthA"
"hideA"
"menuButtonA"
"closeI"
"maximizeI"
"restoreI"
"minimizeI"
"rollupI"
"rolldownI"
"depthI"
"hideI"
"menuButtonI"
)
# Taskbar slices
taskbar_slices=(
"icewm"
"taskbarbg"
"desktop"
"taskbuttonbg"
"taskbuttonactive"
"taskbuttonminimized"
)
# Frame related
frame_basic_slices=(
"frameATL"
"frameITL"
)
# Menu related
menu_slices=(
"menusel"
)
# Icon slices
icon_slices=(
"folder"
"windows"
"settings"
"help"
"focus"
"pref"
"key"
"save"
"themes"
"logout"
"reboot"
"shutdown"
"suspend"
"restart"
"xterm"
"app"
"xv"
)
# All slices, including window slices, taskbar slices, icon slices etc.
slices=(
"${window_slices[@]}"
"${taskbar_slices[@]}"
"${frame_basic_slices[@]}"
"${menu_slices[@]}"
"${icon_slices[@]}"
)
# LayerPrev calls are to select the first layer. "LayerNext; LayerSolo;" will keep only second layer showing.
actions="LayerPrev; LayerPrev; LayerPrev; LayerNext; LayerSolo; "
for slice in "${slices[@]}"; do
	actions="${actions}export-id:${slice}; export-filename:${export_dir}/${slice}.png; export-do; "
done
inkscape --actions="$actions" --batch-process theme.svg

# Process theme dir xpm images
theme_root_xpm_slices=(
"${window_slices[@]}"
"${frame_basic_slices[@]}"
"${menu_slices[@]}"
)
mkdir -p "${theme_path}"
for slice in "${theme_root_xpm_slices[@]}"; do
	convert_image "${export_dir}/${slice}.png" "${theme_path}/${slice}.xpm"
done

# Prep taskbar images
mkdir -p "${theme_path}/taskbar"
for slice in "${taskbar_slices[@]}"; do
	convert_image "${export_dir}/${slice}.png" "${theme_path}/taskbar/${slice}.xpm"
done
# Same image
cp "${theme_path}/taskbar/taskbarbg.xpm" "${theme_path}/taskbar/toolbuttonbg.xpm"

# Copy pixel to other border images
echo "= prepping border images"
# T=Top
# R=Right
# B=Bottom
# L=Left
# A=Active window
# I=Inactive window
# Starting clockwise from top-left, top tile, top-right, right tile etc.
border_image_suffixes=(
TL
T
TR
R
BR
B
BL
L
)
# trad theme is simple, so we need only a single pixel for border on all sides.
# So the same image can be copied to all images needed (without generating
# slice images for each of them). We do this for active [A] and then
# inactive [I] window. "frame" series is for normal windows and "dframe" is for
# dialog windows.
# So we need images for:
# - Normal window (active) [frameA$suff]
# - Normal window (inactive) [frameI$suff]
# - Dialog window (active) [dframeA$suff]
# - Dialog window (inactive) [dframeI$suff]
for suff in "${border_image_suffixes[@]}"; do
	cp "${theme_path}/frameATL.xpm" "${theme_path}/frameA${suff}.xpm"
	cp "${theme_path}/frameATL.xpm" "${theme_path}/dframeA${suff}.xpm"
	cp "${theme_path}/frameITL.xpm" "${theme_path}/frameI${suff}.xpm"
	cp "${theme_path}/frameITL.xpm" "${theme_path}/dframeI${suff}.xpm"
done

# Prepare icon images
mkdir -p "${icewm_config_path}/icons"
for slice in "${icon_slices[@]}"; do
	mv "${export_dir}/${slice}.png" "${icewm_config_path}/icons/${slice}_32x32.png"
	convert_image_resize "${icewm_config_path}/icons/${slice}_32x32.png" "${icewm_config_path}/icons/${slice}_16x16.png" 16 16
done

# Wallpaper
echo "= prepping wallpaper"
inkscape --actions="export-area-snap; export-filename:${theme_path}/default.png; export-do;" --batch-process wallpaper.svg
